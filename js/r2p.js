require([
  "esri/Map",
  "esri/Basemap",
  "esri/Graphic",
  "esri/views/MapView",
  "esri/layers/FeatureLayer",
  "esri/layers/GraphicsLayer",
  "esri/layers/TileLayer",
  "esri/request",
  "esri/geometry/support/webMercatorUtils",
  "esri/geometry/Point",
  "esri/widgets/BasemapToggle",
  "dojo/domReady!"
], function(
  Map,
  Basemap,
  Graphic,
  MapView,
  FeatureLayer,
  GraphicsLayer,
  TileLayer,
  esriRequest,
  WebMercatorUtils,
  Point,
  BasemapToggle
) {
  var g2mUrl =
    "https://gis.transportation.wv.gov/arcgis/rest/services/Roads_And_Highways/Publication_LRS/MapServer/exts/LRSServer/networkLayers/89/geometryToMeasure";

  var graphicsLayer = new GraphicsLayer();
  var sheriffLayer = new TileLayer({
    url: "https://tagis.dep.wv.gov/arcgis/rest/services/webMercator/WVhillshade_wm/MapServer"
  });

  var sheriff = new Basemap({
    baselayers: [sheriffLayer],
    title: "Sheriff",
    id: "Sheriff",
    thumbnailUrl: "https://stamen-tiles.a.ssl.fastly.net/terrain/10/177/409.png"
  });

  var map = new Map({
    basemap: "streets"
  });

  var r2pSym = {
    type: "simple",
    symbol: {
      type: "simple-line",
      width: 3.75,
      color: [255, 0, 0, 1],
      style: "solid"
    }
  };

  function drawSymbol(x, y) {
    console.log(`${x}, ${y}`);
    var pointSymbol = {
      type: "simple-marker",
      style: "circle",
      outline: {
        cap: "round"
      },
      angle: 0,
      size: 24,
      color: [0, 255, 0, 0.8]
    };
    var crossSymbol = {
      type: "simple-marker",
      style: "cross",
      outline: {
        cap: "round"
      },
      angle: 0,
      size: 24,
      color: [255, 255, 255, 0.8]
    };

    graphicsLayer.removeAll();

    var point = new Point({
      longitude: x,
      latitude: y
    });

    var crossGraphic = new Graphic({
      geometry: point,
      symbol: crossSymbol
    });
    pointGraphic = new Graphic({
      geometry: point,
      symbol: pointSymbol
    });

    graphicsLayer.add(pointGraphic);
    graphicsLayer.add(crossGraphic);
  }

  var milePoint = "Click not on Route";
  var mpX, mpY;

  function g2mMilepoint(target) {
    console.log("target", target);
    var content;
    //  milePoint = getMilepoint(mpX, mpY);
    var routeId = target.graphic.attributes.Routeid;
    var countyCode = routeId.substring(0, 2);
    var countyName, countyURL;
    countyURL = "https://gis.transportation.wv.gov/api/counties/" + countyCode;
    var county = getCounty(countyCode);
    var route = routeId.substring(3, 7).replace(/^0+/, "");
    var subroute = routeId.substring(7, 9).replace(/^0+/, "");
    var signSystem = getSignSystem(routeId.substring(2, 3));
    var supplementalCode = getSupplementalCode(routeId.substring(9, 11));
    var direction = getDirection(routeId.substring(11, 13));
    return `<ul><li>County: <b>${county}</b></li><li>ProjNO: <b>{ProjNO}</b></li><li>Route: <b>${route}</b></li><li>Subroute: <b>${subroute}</b></li><li>Sign System: <b>${signSystem}</b></li><li>Supp Code: <b>${supplementalCode}</b></li><li>Direction: <b>${direction}</b></li><li>PGMCDE: <b>{PGMCDE}</b></li><li>Milepoint at click: <b>${milePoint}</b></li><li>StartMP: <b>{StartMP}</b></li><li>Length: <b>{Length}</b></li><li>endmp: <b>{endmp}</b> </li><li>commentsMB: <b>{commentsMB}</b></li><li>commentDis: <b>{commentDis}</b></li><li>STRD1:<b> {STRD1}</b></li></ul>`;
  }

  var r2pLayer = new FeatureLayer({
    url:
      "https://gis.transportation.wv.gov/arcgis/rest/services/Projects/r2p/MapServer/0",
    renderer: r2pSym,
    outFields: ["*"],
    popupTemplate: {
      title: "Route ID - {routeidnew}",
      content: g2mMilepoint
    }
  });

  map.add(r2pLayer);
  map.add(graphicsLayer);

  var view = new MapView({
    container: "viewDiv",
    map: map,
    zoom: 6,
    center: [-80.65679, 38.9342] // longitude, latitude
  });
  view.padding = {
    top: 50,
    bottom: 0
  };

  view.popup.set("dockOptions", {
    position: "bottom-left"
  });

  view.when(() => {
    var toggle = new BasemapToggle({
      view: view,
      nextBasemap: "hybrid"
    });
    view.ui.add(toggle, "bottom-right");
  });

  // view.ui.add("resultsDiv", "top-right");

  view.on("click", event => {
    mpX = event.mapPoint.x;
    mpY = event.mapPoint.y;
    // console.log(JSON.stringify(event));

    // var screenPoint = {
    //   x: event.x,
    //   y: event.y
    // };
    // console.log(`X: ${event.mapPoint.x}, Y: ${event.mapPoint.y}`);
    // view.hitTest(screenPoint).then(response => {
    //   if (response.results.length) {
    //     var graphic = response.results.filter(result => {
    //       return result.graphic.layer === r2pLayer;
    //     })[0].graphic;
    //     console.log(graphic.attributes);
    var lngLat = WebMercatorUtils.xyToLngLat(
      event.mapPoint.x,
      event.mapPoint.y
    );
    //     var measureLocation = {};
    //     var geometry = {};
    //     geometry.x = event.mapPoint.x;
    //     geometry.y = event.mapPoint.y;
    //     measureLocation.geometry = geometry;
    //     locations = [measureLocation];
    //     options = {
    //       query: {
    //         locations: JSON.stringify(locations),
    //         inSR: view.spatialReference.wkid,
    //         outSR: view.spatialReference.wkid,
    //         tolerance: 10,
    //         f: "json"
    //       },
    //       responseType: "json"
    //     };
    //     esriRequest(g2mUrl, options).then(function(response) {
    //       console.log("response.data", response.data);
    //       if (
    //         response.data.locations[0].status === "esriLocatingOK" ||
    //         response.data.locations[0].status === "esriLocatingMultipleLocation"
    //       ) {
    try {
      drawSymbol(lngLat[0], lngLat[1]);
    } catch (e) {
      console.log("unable to draw symbol", e);
    }

    //         var resultsHeader = dojo.byId("resultsHeader");
    //         var resultsInner = dojo.byId("resultsInner");
    //         resultsInner.innerHTML =
    //           "<p>Milepoint at click: " +
    //           response.data.locations[0].results[0].measure.toFixed(3) +
    //           "</p>";

    //           console.log("puTemp",r2pLayer.popupTemplate.content);
    //         // r2pLayer.popupTemplate.content +=
    //         //   "Milepoint at click: " +
    //         //     response.data.locations[0].results[0].measure.toFixed(3);

    //       }
    getMilepoint(mpX, mpY);
    console.log("lnglat", lngLat);
    view.goTo({
      center: [lngLat[0], lngLat[1]],
      zoom: 14
    });
  });

  // });

  function getMilepoint(x, y) {
    console.log(`getMilepoint(${x},${y})`);
    var screenPoint = {
      x: x,
      y: y
    };
    // console.log(`X: ${event.mapPoint.x}, Y: ${event.mapPoint.y}`);
    // view.hitTest(screenPoint).then(response => {
    //   if (response.results.length) {
    //     var graphic = response.results.filter(result => {
    //       return result.graphic.layer === r2pLayer;
    //     })[0].graphic;
    //     console.log(graphic.attributes);
    //     var lngLat = WebMercatorUtils.xyToLngLat(
    //       event.mapPoint.x,
    //       event.mapPoint.y
    //     );
    var measureLocation = {};
    var geometry = {};
    geometry.x = x;
    geometry.y = y;
    measureLocation.geometry = geometry;
    locations = [measureLocation];
    options = {
      query: {
        locations: JSON.stringify(locations),
        inSR: view.spatialReference.wkid,
        outSR: view.spatialReference.wkid,
        tolerance: 10,
        f: "json"
      },
      responseType: "json"
    };
    esriRequest(g2mUrl, options).then(function(response) {
      console.log("response.data", response.data);
      if (
        response.data.locations[0].status === "esriLocatingOK" ||
        response.data.locations[0].status === "esriLocatingMultipleLocation"
      ) {
        var resultsHeader = dojo.byId("resultsHeader");
        var resultsInner = dojo.byId("resultsInner");
        // console.log("puTemp",r2pLayer.popupTemplate.content);
        milePoint = response.data.locations[0].results[0].measure.toFixed(3);

        // r2pLayer.popupTemplate.content +=
        //   "Milepoint at click: " +
        //     response.data.locations[0].results[0].measure.toFixed(3);
      } else {
        milePoint = "Click not on route";
      }
    });
  }

  function getCounty(countyCode) {
    var counties = [
      {
        CountyCode: "01",
        CountyName: "Barbour"
      },
      {
        CountyCode: "02",
        CountyName: "Berkeley"
      },
      {
        CountyCode: "03",
        CountyName: "Boone"
      },
      {
        CountyCode: "04",
        CountyName: "Braxton"
      },
      {
        CountyCode: "05",
        CountyName: "Brooke"
      },
      {
        CountyCode: "06",
        CountyName: "Cabell"
      },
      {
        CountyCode: "07",
        CountyName: "Calhoun"
      },
      {
        CountyCode: "08",
        CountyName: "Clay"
      },
      {
        CountyCode: "09",
        CountyName: "Doddridge"
      },
      {
        CountyCode: "10",
        CountyName: "Fayette"
      },
      {
        CountyCode: "11",
        CountyName: "Gilmer"
      },
      {
        CountyCode: "12",
        CountyName: "Grant"
      },
      {
        CountyCode: "13",
        CountyName: "Greenbrier"
      },
      {
        CountyCode: "14",
        CountyName: "Hampshire"
      },
      {
        CountyCode: "15",
        CountyName: "Hancock"
      },
      {
        CountyCode: "16",
        CountyName: "Hardy"
      },
      {
        CountyCode: "17",
        CountyName: "Harrison"
      },
      {
        CountyCode: "18",
        CountyName: "Jackson"
      },
      {
        CountyCode: "19",
        CountyName: "Jefferson"
      },
      {
        CountyCode: "20",
        CountyName: "Kanawha"
      },
      {
        CountyCode: "21",
        CountyName: "Lewis"
      },
      {
        CountyCode: "22",
        CountyName: "Lincoln"
      },
      {
        CountyCode: "23",
        CountyName: "Logan"
      },
      {
        CountyCode: "24",
        CountyName: "McDowell"
      },
      {
        CountyCode: "25",
        CountyName: "Marion"
      },
      {
        CountyCode: "26",
        CountyName: "Marshall"
      },
      {
        CountyCode: "27",
        CountyName: "Mason"
      },
      {
        CountyCode: "28",
        CountyName: "Mercer"
      },
      {
        CountyCode: "29",
        CountyName: "Mineral"
      },
      {
        CountyCode: "30",
        CountyName: "Mingo"
      },
      {
        CountyCode: "31",
        CountyName: "Monongalia"
      },
      {
        CountyCode: "32",
        CountyName: "Monroe"
      },
      {
        CountyCode: "33",
        CountyName: "Morgan"
      },
      {
        CountyCode: "34",
        CountyName: "Nicholas"
      },
      {
        CountyCode: "35",
        CountyName: "Ohio"
      },
      {
        CountyCode: "36",
        CountyName: "Pendleton"
      },
      {
        CountyCode: "37",
        CountyName: "Pleasants"
      },
      {
        CountyCode: "38",
        CountyName: "Pocahontas"
      },
      {
        CountyCode: "39",
        CountyName: "Preston"
      },
      {
        CountyCode: "40",
        CountyName: "Putnam"
      },
      {
        CountyCode: "41",
        CountyName: "Raleigh"
      },
      {
        CountyCode: "42",
        CountyName: "Randolph"
      },
      {
        CountyCode: "43",
        CountyName: "Ritchie"
      },
      {
        CountyCode: "44",
        CountyName: "Roane"
      },
      {
        CountyCode: "45",
        CountyName: "Summers"
      },
      {
        CountyCode: "46",
        CountyName: "Taylor"
      },
      {
        CountyCode: "47",
        CountyName: "Tucker"
      },
      {
        CountyCode: "48",
        CountyName: "Tyler"
      },
      {
        CountyCode: "49",
        CountyName: "Upshur"
      },
      {
        CountyCode: "50",
        CountyName: "Wayne"
      },
      {
        CountyCode: "51",
        CountyName: "Webster"
      },
      {
        CountyCode: "52",
        CountyName: "Wetzel"
      },
      {
        CountyCode: "53",
        CountyName: "Wirt"
      },
      {
        CountyCode: "54",
        CountyName: "Wood"
      },
      {
        CountyCode: "55",
        CountyName: "Wyoming"
      }
    ];

    for (var i = 0; i < counties.length; i++) {
      if (counties[i].CountyCode == countyCode) {
        console.log(counties[i].CountyName);
        return counties[i].CountyName;
      }
    }
  }

  function getSignSystem(signSystemCode) {
    console.log("ssCode", signSystemCode);
    var signSys;
    switch (signSystemCode) {
      case "1":
        signSys = "Interstate";
        break;
      case "2":
        signSys = "US";
        break;
      case "3":
        signSys = "WV";
        break;
      case "4":
        signSys = "CO";
        break;
      case "5":
        signSys = "N/A";
        break;
      case "6":
        signSys = "State Park / Forest Rd";
        break;
      case "7":
        signSys = "FANS";
        break;
      case "8":
        signSys = "HARP";
        break;
      case "9":
        signSys = "OTHER";
        break;
      case "0":
        signSys = "MNS";
        break;
    }
    console.log("signSys", signSys);
    //  $("#resultSignSys").html(signSys);
    return signSys;
  }

  function getSupplementalCode(supplementalCode) {
    console.log("supplementalCode", supplementalCode);
    var suppCode;
    switch (supplementalCode) {
      case "00":
        suppCode = "N/A";
        break;
      case "01":
        suppCode = "Alternate";
        break;
      case "02":
        suppCode = "Wye";
        break;
      case "03":
        suppCode = "Spur";
        break;
      case "04":
        suppCode = "North";
        break;
      case "05":
        suppCode = "South";
        break;
      case "06":
        suppCode = "East";
        break;
      case "07":
        suppCode = "West";
        break;
      case "08":
        suppCode = "Business";
        break;
      case "09":
        suppCode = "North Bound (Business)";
        break;
      case "10":
        suppCode = "South Bound (Business)";
        break;
      case "11":
        suppCode = "East Bound (Business)";
        break;
      case "12":
        suppCode = "West Bound (Business)";
        break;
      case "13":
        suppCode = "Truck Route";
        break;
      case "14":
        suppCode = "Bypass";
        break;
      case "15":
        suppCode = "Loop";
        break;
      case "16":
        suppCode = "Toll";
        break;
      case "17":
        suppCode = "Ramp";
        break;
      case "18":
        suppCode = "Other";
        break;
      case "19":
        suppCode = "City Streets Non-State";
        break;
      case "20":
        suppCode = "Roads Under construction";
        break;
      case "21":
        suppCode = "Footbridges";
        break;
      case "22":
        suppCode = "Historical Bridges";
        break;
      case "23":
        suppCode = "Connector";
        break;
      case "24":
        suppCode = "New/Proposed";
        break;
      case "25":
        suppCode = "Crossover (btwn dual geometry)";
        break;
      case "26":
        suppCode = "Emergency Crossover";
        break;
      case "27":
        suppCode = "Left Turn Lane";
        break;
      case "28":
        suppCode = "Right Turn Lane";
        break;
      case "99":
        suppCode = "Abandoned";
        break;
    }
    console.log("suppCode", suppCode);
    return suppCode;
  }

  function getDirection(direction) {
    switch (direction) {
      case "00":
        direction = "Undivided";
        break;
      default:
        direction = direction;
        break;
    }
    return direction;
  }
});
