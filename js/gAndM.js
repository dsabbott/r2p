var map, view, zoomLevel, newLayerDescription, pointGraphic, lngLat;
var searchType = "Milepoint";

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return "";
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

require([
  "esri/Map",
  "esri/Basemap",
  "esri/geometry/Extent",
  "esri/geometry/Point",
  "esri/geometry/Polyline",
  "esri/geometry/Multipoint",
  "esri/geometry/SpatialReference",
  "esri/views/MapView",
  "esri/layers/FeatureLayer",
  "esri/layers/MapImageLayer",
  "esri/layers/GraphicsLayer",
  "esri/layers/TileLayer",
  "esri/layers/WebTileLayer",
  "esri/renderers/UniqueValueRenderer",
  "esri/renderers/SimpleRenderer",
  "esri/symbols/SimpleLineSymbol",
  "esri/symbols/SimpleMarkerSymbol",
  "esri/symbols/TextSymbol",
  "esri/Graphic",
  "esri/request",
  "esri/geometry/support/webMercatorUtils",
  "esri/widgets/Search",
  "esri/widgets/Locate",
  "esri/widgets/Legend",
  "dojo/dom",
  "dojo/on",
  "dojo/domReady!"
], function(
  Map,
  Basemap,
  Extent,
  Point,
  Polyline,
  Multipoint,
  SpatialReference,
  MapView,
  FeatureLayer,
  MapImageLayer,
  GraphicsLayer,
  TileLayer,
  WebTileLayer,
  UniqueValueRenderer,
  SimpleRenderer,
  SimpleLineSymbol,
  SimpleMarkerSymbol,
  TextSymbol,
  Graphic,
  esriRequest,
  WebMercatorUtils,
  Search,
  Locate,
  Legend,
  dom,
  on
) {
  var customLayerURL = getParameterByName("layer");
  var customLayerType = getParameterByName("type");
  var legend;

  var measuresAPIurl = "https://gis.transportation.wv.gov/api/measures/";

  view = new MapView();

  var baseUrl =
    "https://gis.transportation.wv.gov/arcgis/rest/services/Roads_And_Highways/Publication_LRS/MapServer/exts/LRSServer/networkLayers/89/";
  var lrsData;

  // Map Layers
  // ----------------------------------------------------------------------------
  // var hsLayer = new TileLayer({
  //   url:
  //     "https://tagis.dep.wv.gov/arcgis/rest/services/webMercator/WVhillshade_wm/MapServer",
  //   //url: "https://basemap.nationalmap.gov/arcgis/rest/services/USGSShadedReliefOnly/MapServer",
  //   opacity: 0.6
  // });

  var statesLayer = new MapImageLayer({
    url:
      "https://gis.transportation.wv.gov/arcgis/rest/services/Boundaries/MapServer",
    sublayers: [
      {
        id: 15,
        visible: true
      }
    ]
  });

  var satLayer = new TileLayer({
    url:
      "https://services.wvgis.wvu.edu/arcgis/rest/services/ImageryBaseMaps/wv_aerial_photos_Sheriff_Assoc_2010_11_1ft_wm/MapServer"
  });

  var hsLayer = satLayer;

  var customLayer;
  if (customLayerURL != null && customLayerType == null) {
    customLayerType = "FeatureLayer";
  }
  if (customLayerType != null) {
    switch (customLayerType) {
      case "TileLayer":
        customLayer = new TileLayer();
        customLayer.url = customLayerURL;
        break;
      case "FeatureLayer":
        customLayer = new FeatureLayer();
        customLayer.url = customLayerURL;
        break;
      case "MapImageLayer":
        customLayer = new MapImageLayer();
        customLayer.url = customLayerURL;
        break;
    }
  }
var r2pSym = 
  {
      type: "simple", 
      symbol: {
        type: "simple-line",
        width: 3.75,
        color: [255,0,0,1],
        style: "solid"
      }
   
  };
  var r2pTemplate = {
    title: "Roads to Prosperity - {routeidnew}",
    content: "<ul><li>ProjNO: {ProjNO}</li><li>PGMCDE: {PGMCDE}</li><li>StartMP: {StartMP}</li><li>Length: {Length}</li><li>endmp: {endmp}</li><li>commentsMB: {commentsMB}</li><li>commentDis: {commentDis}</li><li>COUNTY1NAM: {COUNTY1NAM}</li><li>STRD1: {STRD1}</li></ul>"
};

  var roadsLayer = new FeatureLayer({
    url:
      "https://gis.transportation.wv.gov/arcgis/rest/services/Projects/r2p/MapServer/0",
    //url: "https://gis.transportation.wv.gov/arcgis/rest/services/Base_Maps/RoadCache/MapServer",
    // opacity: 1,
    // id: "WVDOHBasemap",
    renderer: r2pSym,
    popupTemplate: r2pTemplate
  });


  var roadcacheLayer = new TileLayer({
    url:
      "https://gis.transportation.wv.gov/arcgis/rest/services/Base_Maps/RoadCache/MapServer",
    opacity: 1,
    id: "RoadCache"
  });

  var graphicsLayer = new GraphicsLayer(); // for the "Click" events
  var lineLayer = new GraphicsLayer(); // for the Route To Measure

  // --------------------------------------------------------------------------------

  // Set up the UI
  // -----------------------------------------------------------------------------

  $("#iconSatellite").hide(); // Satellite checkmark in Basemap menu
  $("#iconRoadCache").hide(); // RoadCache checkmark in Roads Layer menu

  $("#RouteIDRoute").hide(); // For error display in Route ID Builder

  $("#resultsDiv").hide(); // Query results

  // Default Extent
  var ext = new Extent({
    xmin: -9560409,
    ymin: 4413943,
    xmax: -8396731,
    ymax: 5068244,
    spatialReference: new SpatialReference({
      wkid: 102100
    })
  });

  var searchWidget = new Search({
    view: view,
    popupOpenOnSelect: false
  });

  view.ui.add(searchWidget, {
    position: "top-left",
    index: 0
  });

  var locator = new Locate({
    view: view
  });
  view.ui.add(locator, {
    position: "top-left"
  });

  //view.then(createLegend());

  function createLegend() {
    /*  try {
            view.ui.remove(legend);
        } catch (error) {
            console.log(error);
        }
        if (typeof map != "undefined") { 
        var featureLyr = map.layers.getItemAt(1);
        legend = new Legend({
            view:view,
            layerInfos : [{
                layer: featureLyr,
                title: "Legend"
            }]
        });
        view.ui.add(legend, "bottom-right"); 
        
    }*/
    view.ui.add("legendDiv", "bottom-right");
  }
  // ------------------------------------------------------------------------------

  // Events
  // -----------------------------------------------------------------------------
  // Opens Route ID Builder
  $("#btnRouteIdBuilder").click(function() {
    $("#RouteIDRoute").hide();
    $("#LRSModal").modal();
  });

  $("#btnHelp").click(function() {
    $("#HelpModal").modal();
  });

  // Closes Route ID Builder
  $("#btnRouteID").click(function() {
    if ($("#RouteIDRouteNumber").val() == "") {
      $("#RouteIDRoute")
        .text("Route Number must be > 0000")
        .show();
      // } else if ($("#RouteIDRouteNumber").val().length != 4) {
      // $("#RouteIDRoute").text("Route Number must 4 digits").show();
    } else {
      padRoute(4, $("#RouteIDRouteNumber").val(), RouteIDRouteNumber);
      padRoute(2, $("#RouteIDSubroute").val(), RouteIDSubroute);
      $("#RouteIDRoute").hide();
      $("#routeID").val(
        $("#RouteIDCounty").val() +
          $("#RouteIDSignSystem").val() +
          $("#RouteIDRouteNumber").val() +
          $("#RouteIDSubroute").val() +
          $("#RouteIDSuppCode").val() +
          $("#RouteIDDirection").val()
      );
      $("#LRSModal").modal("hide");
    }
  });

  // Clear map
  var btnClear = $("#btnClear");
  btnClear.click(function() {
    lineLayer.removeAll();
    graphicsLayer.removeAll();
  });

  // Locate by Milepoint or Long/Lat
  var btnLocate = $("#btnLocate");
  btnLocate.click(function() {
    lineLayer.removeAll();
    graphicsLayer.removeAll();
    var data = {};
    switch (searchType) {
      case "LatLong":
        /*
                    findLocation(searchType, $("#endMeasure").val(), $("#beginMeasure").val(), $("#routeID").val().toUpperCase());
                    var xy = WebMercatorUtils.lngLatToXY($("#endMeasure").val(), $("#beginMeasure").val());
                    getLRSData("geometryToMeasure", xy[0], xy[1]);
                    */
        if ($("#beginMeasure").val() != "" && $("#endMeasure").val() != "") {
          data.X = $("#beginMeasure")
            .val()
            .trim();
          data.Y = $("#endMeasure")
            .val()
            .trim();
          data.LRSOp = "geometryToMeasure";
          data.eventType = "LatLong";
          data.Color = "green";
          drawSinglePoint(data);
          var xy = WebMercatorUtils.lngLatToXY(data.X, data.Y);
          data.X = xy[0];
          data.Y = xy[1];
          getLRSData(data);
        } else {
          alert("Your Lat Long is Wrong. Try again.");
        }
        break;
      case "Milepoint":
        if ($("#routeID").val().length < 13) {
          $("#routeError")
            .show()
            .html("<p>Route ID must be 13 characters</p>");
        } else {
          var route = $("#routeID")
            .val()
            .toUpperCase();
          var beginMeasure = $("#beginMeasure").val();
          if (beginMeasure == "") {
            beginMeasure = "0";
          }
          var endMeasure = $("#endMeasure").val();
          if (endMeasure != "" && beginMeasure > endMeasure) {
            var beg, end;
            end = beginMeasure;
            beg = endMeasure;
            beginMeasure = beg;
            endMeasure = end;
            $("#beginMeasure").val(beginMeasure);
            $("#endMeasure").val(endMeasure);
          }
          $("#routeError").hide();

          data.X = endMeasure;
          data.Y = beginMeasure;
          data.routeID = route;
          data.LRSOp = "measureToGeometry";
          data.Color = "green";
          data.eventType = "Milepoint";
          getLRSData(data);
          // updateMap(data);
          //  getLRSData("measureToGeometry", endMeasure, beginMeasure, route);

          //  updateMap(lrsData.locations[0].geometry.x, lrsData.locations[0].geometry.y);
        }
        break;
    }
    $(".esri-legend").toggle();
  });

  // Display the Add Layer modal
  $("#btnShowAddLayer").click(function() {
    $("#AddLayerModal").modal();
  });

  $("#RouteToMeasureToggle").click(function() {
    if ($("#iconRouteID").is(".glyphicon-resize-full")) {
      $("#iconRouteID")
        .removeClass("glyphicon-resize-full")
        .addClass("glyphicon-resize-small");
    } else {
      $("#iconRouteID")
        .removeClass("glyphicon-resize-small")
        .addClass("glyphicon-resize-full");
    }
  });
  $("#searchTitle").click(function() {
    if ($("#iconRouteID").is(".glyphicon-resize-full")) {
      $("#iconRouteID")
        .removeClass("glyphicon-resize-full")
        .addClass("glyphicon-resize-small");
    } else {
      $("#iconRouteID")
        .removeClass("glyphicon-resize-small")
        .addClass("glyphicon-resize-full");
    }
  });

  //$("#btnIconResults").click(function()  {
  function toggleIconResults() {
    console.log("btnIconResults");
    //  $("#resultsDivInner").toggle();
    //  $("#resultsTitle").toggle();
    if ($("#iconResults").is(".glyphicon-resize-full")) {
      $("#iconResults")
        .removeClass("glyphicon-resize-full")
        .addClass("glyphicon-resize-small");
    } else {
      $("#iconResults")
        .removeClass("glyphicon-resize-small")
        .addClass("glyphicon-resize-full");
    }
  }

  $("input[type=radio][name=SearchType").change(function() {
    if (this.value == "LatLong") {
      console.log(this.value);
      $("#routeID").prop("disabled", true);
      $("#btnRouteIdBuilder").prop("disabled", true);
      $("#routeID")
        .attr("placeholder", "")
        .val("");
      $("#beginMeasure")
        .attr("placeholder", "Longitude -00.000000")
        .val("");
      $("#endMeasure")
        .attr("placeholder", "Latitude 00.000000")
        .val("");
    } else {
      console.log(this.value);
      $("#routeID").prop("disabled", false);
      $("#btnRouteIdBuilder").prop("disabled", false);
      $("#routeID").attr("placeholder", "Route ID (required)");
      $("#beginMeasure")
        .attr("placeholder", "Begin Milepoint 0.000")
        .val("");
      $("#endMeasure")
        .attr("placeholder", "End Milepoint 0.000")
        .val("");
    }
    searchType = this.value;
  });

  $("#RouteIDCounty").change(function() {
    var county = $("#RouteIDCounty").val();
    if (county != "NA") {
      $("#RouteIDSignSystem")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDRouteNumber")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDSubroute")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDSuppCode")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDDirection")
        .children("option:not(:first)")
        .remove();
      var url = measuresAPIurl + county;
      $.getJSON(url, null, function(data) {
        console.log("api Data", data);
        var signSysData = {};
        $.each(data, function(key, value) {
          $("#RouteIDSignSystem").append(
            $("<option></option>")
              .attr("value", value.CO_SignSystem)
              .text(getSignSystem(value.CO_SignSystem))
          );
        });
      });

      $("#RouteIDSignSystem").prop("disabled", false);
    }
  });

  $("#RouteIDSignSystem").change(function() {
    var county = $("#RouteIDCounty").val();
    var signSys = $("#RouteIDSignSystem").val();
    if (signSys != "NA") {
      $("#RouteIDRouteNumber")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDSubroute")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDSuppCode")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDDirection")
        .children("option:not(:first)")
        .remove();
      var url = measuresAPIurl + county + "/" + signSys;
      $.getJSON(url, null, function(data) {
        console.log("api Data", data);
        var signSysData = {};
        $.each(data, function(key, value) {
          $("#RouteIDRouteNumber").append(
            $("<option></option>")
              .attr("value", value.CO_RouteNumber)
              .text(value.CO_RouteNumber)
          );
        });
      });

      $("#RouteIDRouteNumber").prop("disabled", false);
    }
  });

  $("#RouteIDRouteNumber").change(function() {
    var county = $("#RouteIDCounty").val();
    var signSys = $("#RouteIDSignSystem").val();
    var routeNo = $("#RouteIDRouteNumber").val();
    if (routeNo != "NA") {
      $("#RouteIDSubroute")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDSuppCode")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDDirection")
        .children("option:not(:first)")
        .remove();
      var url = measuresAPIurl + county + "/" + signSys + "/" + routeNo;
      $.getJSON(url, null, function(data) {
        console.log("api Data", data);
        var signSysData = {};
        $.each(data, function(key, value) {
          $("#RouteIDSubroute").append(
            $("<option></option>")
              .attr("value", value.CO_SubRoute)
              .text(value.CO_SubRoute)
          );
        });
      });

      $("#RouteIDSubroute").prop("disabled", false);
    }
  });

  $("#RouteIDSubroute").change(function() {
    var county = $("#RouteIDCounty").val();
    var signSys = $("#RouteIDSignSystem").val();
    var routeNo = $("#RouteIDRouteNumber").val();
    var subroute = $("#RouteIDSubroute").val();
    if (subroute != "NA") {
      $("#RouteIDSuppCode")
        .children("option:not(:first)")
        .remove();
      $("#RouteIDDirection")
        .children("option:not(:first)")
        .remove();
      var url =
        measuresAPIurl +
        county +
        "/" +
        signSys +
        "/" +
        routeNo +
        "/" +
        subroute;
      $.getJSON(url, null, function(data) {
        console.log("api Data", data);
        var signSysData = {};
        $.each(data, function(key, value) {
          //  $("#RouteIDSuppCode").append($("<option></option>").attr("value", value.CO_SuppCode).text(value.CO_SuppCode));
          $("#RouteIDSuppCode").append(
            $("<option></option>")
              .attr("value", value.CO_SuppCode)
              .text(getSupplementalCode(value.CO_SuppCode))
          );
        });
      });

      $("#RouteIDSuppCode").prop("disabled", false);
    }
  });
  $("#RouteIDSuppCode").change(function() {
    var county = $("#RouteIDCounty").val();
    var signSys = $("#RouteIDSignSystem").val();
    var routeNo = $("#RouteIDRouteNumber").val();
    var subroute = $("#RouteIDSubroute").val();
    var suppCode = $("#RouteIDSuppCode").val();
    if (suppCode != "NA") {
      $("#RouteIDDirection")
        .children("option:not(:first)")
        .remove();
      var url =
        measuresAPIurl +
        county +
        "/" +
        signSys +
        "/" +
        routeNo +
        "/" +
        subroute +
        "/" +
        suppCode;
      $.getJSON(url, null, function(data) {
        console.log("api Data", data);
        var signSysData = {};
        $.each(data, function(key, value) {
          $("#RouteIDDirection").append(
            $("<option></option>")
              .attr("value", value.CO_RouteDirection)
              .text(getDirection(value.CO_RouteDirection))
          );
        });
      });

      $("#RouteIDDirection").prop("disabled", false);
    }
  });
  $("#RouteIDDirection").change(function() {
    if ($("#RouteIDDirection").val() != "NA") {
      $("#btnRouteID").prop("disabled", false);
    }
  });

  $("#btnAddLayer").click(function() {
    var newLayerUrl = $("#AddLayerURL").val();
    // console.log("newLayerUrl", newLayerUrl);
    var newLayerType = $("input[name='LayerType']:checked").val();
    console.log("LayerType", newLayerType);
    newLayerDescription = $("#AddLayerDescription").val();
    // console.log("newLayerDescription", newLayerDescription);
    if (newLayerUrl === "") {
      $("#AddLayerURL").after('<p class="error">URL Required</p>');
      return false;
    } else if (newLayerDescription === "") {
      $("#AddLayerDescription").after(
        '<p class="error">Layer Description Required</p>'
      );
      return false;
    } else {
      $("#iconRoadCache").hide();
      $("#iconDOTBM").hide();
      $("p.error").hide();
      if ($("input[name='AddType']:checked").val() === "ReplaceLayer") {
        map.remove(roadsLayer);
        console.log("roadsLayer removed");
      }
      var newLayer;
      switch (newLayerType) {
        case "TileLayer":
          roadsLayer = new TileLayer({
            url: newLayerUrl,
            title: newLayerDescription
          });
          break;
        case "MapImageLayer":
          roadsLayer = new MapImageLayer({
            url: newLayerUrl,
            title: newLayerDescription
          });
          break;
        case "FeatureLayer":
          roadsLayer = new FeatureLayer({
            url: newLayerUrl,
            title: newLayerDescription
          });
      }
      //RemoveCustomLayer(newLayerDescription);
      map.add(roadsLayer);
      map.add(satLayer);
       map.reorder(lineLayer, map.layers.length);
      map.reorder(graphicsLayer, map.layers.length + 1);
      $("#AddLayerModal").modal("hide");
    }
  });

  // Layer events to show Roads Layers and Basemaps
  $("#bmHillshade").click(function() {
    // console.log("map.layers",map.layers);
    $("#iconHillshade").show();
    $("#iconSatellite").hide();
    if (map.findLayerById("RoadCache")) {
      map.removeMany([satLayer, roadcacheLayer]);
      map.add(hsLayer, 0);
      map.add(roadcacheLayer);
      if (customLayerType != null) {
        map.add(customLayer);
      }
    } else if (map.findLayerById("WVDOHBasemap")) {
      map.removeMany([satLayer, roadsLayer]);
      map.add(hsLayer, 0);
      map.add(roadsLayer);
      if (customLayerType != null) {
        map.add(customLayer);
      }
    }
    map.reorder(lineLayer, map.layers.length);
    map.reorder(graphicsLayer, map.layers.length + 1);
  });

  $("#bmSatellite").click(function() {
    $("#iconHillshade").hide();
    $("#iconSatellite").show();
    if (map.findLayerById("RoadCache")) {
      map.removeMany([hsLayer, roadcacheLayer]);
      map.add(satLayer, 0);
      map.add(roadcacheLayer);
      if (customLayerType != null) {
        map.add(customLayer);
      }
    } else if (map.findLayerById("WVDOHBasemap")) {
      map.removeMany([hsLayer, roadsLayer]);
      map.add(satLayer, 0);
      map.add(roadsLayer);
      if (customLayerType != null) {
        map.add(customLayer);
      }
    }
    map.reorder(lineLayer, map.layers.length);
    map.reorder(graphicsLayer, map.layers.length + 1);
  });

  $("#btnRoadCache").click(function() {
    $("#iconRoadCache").show();
    $("#iconDOTBM").hide();
    zoomLevel = 7;
    map.remove(roadsLayer);
    // map.add(roadcacheLayer);
    roadsLayer = new TileLayer({
      url:
        "https://gis.transportation.wv.gov/arcgis/rest/services/Base_Maps/RoadCache/MapServer",
      id: "RoadCache"
    });
    console.log("roadLayer.url", roadsLayer);
    map.add(roadsLayer, 1);
    map.reorder(lineLayer, map.layers.length);
    map.reorder(graphicsLayer, map.layers.length + 1);
    map.reorder(customLayer, map.layers.length + 1);
    view.constraints = {
      maxZoom: zoomLevel,
      minZoom: 1,
      rotationEnabled: false
    };
    //createLegend();
    // RemoveCustomLayer(newLayerDescription);
  });

  $("#btnWVDOTBasemap").click(function() {
    $("#iconDOTBM").show();
    $("#iconRoadCache").hide();
    zoomLevel = 9;
    map.remove(roadsLayer);
    roadsLayer = new TileLayer({
      url:
        "https://gis.transportation.wv.gov/arcgis/rest/services/Base_Maps/WVDOT_Base_Map_WM/MapServer",
      //url: "https://gis.transportation.wv.gov/arcgis/rest/services/Base_Maps/RoadCache/MapServer",
      opacity: 1,
      id: "WVDOHBasemap"
    });
    map.add(roadsLayer, 1);
    map.reorder(lineLayer, map.layers.length);
    map.reorder(graphicsLayer, map.layers.length + 1);
    map.reorder(customLayer, map.layers.length + 1);
    view.constraints = {
      maxZoom: zoomLevel + 2,
      minZoom: 1,
      rotationEnabled: false
    };
    //  createLegend();
    //  RemoveCustomLayer(newLayerDescription);
  });

  /*  view.on("click", function (event) {
          graphicsLayer.removeAll();
          lineLayer.removeAll();
          findLocation("MapClick", event.mapPoint.x, event.mapPoint.y, "");
          getLRSData("geometryToMeasure", event.mapPoint.x, event.mapPoint.y);
          //  var locs = responseData.locations;
          //  console.log("locs", locs);



      });
      */
  view.on("click", function(event) {
    // view.hitTest(event.screenPoint) 
    //   .then((response) => {
    //     var graphic = response.results[0].graphic;
    //     if (graphic) {
    //       console.log('Graphic', graphic.attributes);
    //     }
    //   });
    graphicsLayer.removeAll();
    lineLayer.removeAll();
    var data = {};
    data.X = event.mapPoint.x;
    data.Y = event.mapPoint.y;
    data.eventType = "MapClick";
    data.LRSOp = "geometryToMeasure";
    data.Color = "green";
    console.log("click data", data);
    drawSinglePoint(data);
    getLRSData(data);
    $(".esri-legend").toggle();
  });

  // ---------------------------------------------------------------------------------

  map = new Map({
    layers: [hsLayer, roadsLayer, statesLayer] //,
    //basemap: "gray-vector"
  });
  if (customLayerURL != null) {
    map.add(customLayer);
  }
  map.add(graphicsLayer);

  view.container = "viewDiv";
  view.map = map;
  view.center = [-81.125537, 35.863534];
  view.zoom = 4;
  view.extent = ext;
  view.padding = {
    top: 50,
    bottom: 0
  };

  view.constraints = {
    minZoom: 1,
    rotationEnabled: false
  };

  var fivePixels = view.extent.width / view.width * 5;

  view.ui.add("resultsDiv", "top-right");
  //view.ui.add("legendDiv", "bottom-right");

  // Utility functions
  // -------------------------------------------------------------
  /* function RemoveCustomLayer(title) {
        var layer = map.allLayers.find(function (layer) {
            return layer.title === title;
        });
        if (layer != null) {
            console.log("layer to remove", layer);

            map.remove(layer);
        }

    }
 */
  function drawPolyline(data) {
    console.log("Draw Polyline", data);
    var locs = data.locations;
    var pathSegs = locs[0].geometry.paths[0].length;
    var paths = [[]];
    console.log("pathSegs", pathSegs);
    for (var j = 0; j < pathSegs; j++) {
      var longLat = WebMercatorUtils.xyToLngLat(
        locs[0].geometry.paths[0][j][0],
        locs[0].geometry.paths[0][j][1]
      );
      console.log("longLat " + j, longLat);
      var path = [];
      path.push(
        longLat[0],
        longLat[1],
        locs[0].geometry.paths[0][j][2],
        locs[0].geometry.paths[0][j][3]
      );
      console.log("path", path);
      paths[0].push(path);
      console.log("paths", paths);
      if (j == pathSegs - 1) {
        var endPoint = {};
        endPoint.X = paths[0][j][0];
        endPoint.Y = paths[0][j][1];
        endPoint.eventType = "LatLong";
        endPoint.Color = "red";
        console.log("Red Milepoint", paths[0][j][3], paths[0][j][3].toFixed(3));
        endPoint.Milepoint = paths[0][j][3].toFixed(3);
        console.log(JSON.stringify(endPoint));
        drawSinglePoint(endPoint);
      }
    }
    var geometry = new Polyline({
      hasZ: true,
      hasM: true,
      paths: paths
    });
    // var lineSymbol = new SimpleLineSymbol({cap: "round", color: {r: 255, g: 0, b:0, a: 0.7}, width: "22px", style: "short-dot"} );
    var lineSymbol = {
      type: "simple-line",
      color: [0, 0, 255, 0.5],
      width: "22px"
    };
    var lineGraphic = new Graphic({
      geometry: geometry,
      symbol: lineSymbol
    });
    graphicsLayer.add(lineGraphic);

    view.goTo(lineGraphic);
  }
  // function updateMap(SearchType, X, Y, RouteID) {
  function drawSinglePoint(data) {
    //function findLocation(response) {
    var X = data.X;
    var Y = data.Y;
    var SearchType = data.eventType;
    console.log("updateMap.data", data);
    zoomLevel = view.zoom > 9 ? view.zoom : "9";
    switch (SearchType) {
      case "MapClick":
      case "LatLong":
        if (X.toString().charAt(3) == ".") {
          console.log("LL: Yes");
          view.goTo(
            {
              target: drawCrosshairs(X, Y, graphicsLayer, data.Color, data.Milepoint),
              zoom: zoomLevel
            },
            {
              duration: 2000
            }
          );
        } else {
          console.log("LL: No");
          lngLat = WebMercatorUtils.xyToLngLat(X, Y);
          view.goTo(
            {
              target: drawCrosshairs(
                lngLat[0],
                lngLat[1],
                graphicsLayer,
                data.Color
              ),
              zoom: zoomLevel
            },
            {
              duration: 2000
            }
          );
        }
        break;
      case "Milepoint":
        $("#routeError").hide();
        lngLat = WebMercatorUtils.xyToLngLat(X, Y);
        view.goTo(
          {
            target: drawCrosshairs(
              lngLat[0],
              lngLat[1],
              graphicsLayer,
              data.Color,
              data.Milepoint
            ),
            zoom: zoomLevel
          },
          {
            duration: 2000
          }
        );
        break;
    }
  }

  function drawCrosshairs(X, Y, grLayer, color, milepoint) {
    var point = new Point({
      longitude: X,
      latitude: Y
    });
    switch (color) {
      default:
        // Green
        color = [0, 255, 0, 0.8];
        break;
      case "red":
        color = [255, 0, 0, 0.8];
        break;
    }

    var pointSymbol = new SimpleMarkerSymbol({
      style: "circle",
      outline: {
        cap: "round"
      },
      angle: 0,
      size: 24,
      color: color
    });
    var crossSymbol = new SimpleMarkerSymbol({
      style: "cross",
      outline: {
        cap: "round"
      },
      angle: 0,
      size: 24,
      color: [255, 255, 255, 0.8]
    });
if (milepoint) {
    var textMilepointSymbol = {
      type: "text",
      color: "black",
      angle: 315,
      text: milepoint,
      xoffset: 20,
      yoffset: 30,
      font: {
        size: 18,
        family: "sans-serif",
        weight: "bolder"
      }
    };

    var milepointGraphic = new Graphic({
      geometry: point,
      symbol: textMilepointSymbol
    });
    
    grLayer.add(milepointGraphic);
  }
    var crossGraphic = new Graphic({
      geometry: point,
      symbol: crossSymbol
    });
    pointGraphic = new Graphic({
      geometry: point,
      symbol: pointSymbol
    });

    grLayer.add(pointGraphic);
    grLayer.add(crossGraphic);

    return pointGraphic;
  }

  function getLRSData(data) {
    var url = baseUrl + data.LRSOp;
    console.log("url", url);
    var measureLocation = {
      routeId: data.routeID
    };
    var locations, options, responseData;
    switch (data.LRSOp) {
      case "geometryToMeasure":
        var geometry = {};
        geometry.x = data.X;
        geometry.y = data.Y;
        measureLocation.geometry = geometry;
        locations = [measureLocation];
        options = {
          query: {
            locations: JSON.stringify(locations),
            inSR: view.spatialReference.wkid,
            outSR: view.spatialReference.wkid,
            tolerance: 10,
            f: "json"
          },
          responseType: "json"
        };
        esriRequest(url, options).then(function(response) {
          console.log("response.data", response.data);
          lrsData = response.data;
          var results = [];
          results.LRSOp = data.LRSOp;
          console.log(
            "response.data.locations[0].results.length",
            response.data.locations[0].results.length
          );
          if (response.data.locations[0].results.length > 0) {
            for (
              var i = 0;
              i < response.data.locations[0].results.length;
              i++
            ) {
              var result = {};
              result.routeId = response.data.locations[0].results[i].routeId;
              result.measure = response.data.locations[0].results[i].measure;
              result.geometry = response.data.locations[0].results[i].geometry;
              result.geometryType =
                response.data.locations[0].results[i].geometryType;
              results.push(result);
            }
          } else {
            var result = {};
            results.push(result);
          }
          // result.routeId = response.data.locations[0].results[0].routeId;
          console.log("results[]", results);

          // console.log("result", result);
          console.log("lrsData", lrsData);

          //showG2MResults(response.data, X, Y);

          showG2MResults(results);
        });
        break;
      case "measureToGeometry":
        if (data.X != "") {
          measureLocation.fromMeasure = data.Y;
          measureLocation.toMeasure = data.X;
        } else {
          measureLocation.measure = data.Y;
        }
        locations = [measureLocation];
        options = {
          query: {
            locations: JSON.stringify(locations),
            outSR: view.spatialReference.wkid,
            f: "json"
          },
          responseType: "json"
        };
        //return esriRequest(url, options);
        esriRequest(url, options).then(function(response) {
          console.log("response.data", response.data);
          lrsData = response.data;
          var data = {};
          switch (lrsData.locations[0].geometryType) {
            case "esriGeometryPoint":
              console.log("esriGeometryPoint");
              data.X = lrsData.locations[0].geometry.x;
              data.Y = lrsData.locations[0].geometry.y;
              data.Milepoint =
                lrsData.locations[0].geometry.m < 0
                  ? 0
                  : lrsData.locations[0].geometry.m;
              data.eventType = "Milepoint";
              data.Color = "green";
              drawSinglePoint(data);
              showM2GResults(lrsData);
              break;
            case "esriGeometryPolyline":
              console.log("esriGeometryPolyline");
              drawPolyline(lrsData);
              data.Y = lrsData.locations[0].geometry.paths[0][0][1];
              data.X = lrsData.locations[0].geometry.paths[0][0][0];
              data.Milepoint = lrsData.locations[0].geometry.paths[0][0][3];
              data.eventType = "Milepoint";
              data.Color = "green";
              console.log("data", data);
              drawSinglePoint(data);
              showM2GResults(lrsData);
              break;
          }

          console.log("data", data);
          //
          // updateMap("Milepoint",lrsData.locations[0].geometry.x, lrsData.locations[0].geometry.y);
          // updateMap(lrsData);

          // showM2GResults(response.data, X, Y);

          // return response.data;
          // console.log("lrsData", lrsData);
        });
      //break;
    }

    console.log("measurelocation", measureLocation);
    console.log("options", options);
    return this;
  }

  function showM2GResults(data) {
    switch (data.locations[0].geometryType) {
      case "esriGeometryPoint":
        console.log("showM2GResults", data);
        console.log("data[0].measure", data.locations[0].geometry.m);
        var measure =
          data.locations[0].geometry.m < 0 ? 0 : data.locations[0].geometry.m;
        console.log("measure", measure);
        var headerContent = "",
          content = "",
          county = "";
        if (measure === undefined) {
          dojo.byId("resultsDivHeader").innerHTML = getHeaderContent(0);
          //   $("#resultsDivInner").hide();
        } else {
          $("#resultsDivInner").show();
          if (data.locations.length >= 1) {
            lngLat = WebMercatorUtils.xyToLngLat(
              data.locations[0].geometry.x,
              data.locations[0].geometry.y
            );
            console.log("lngLat", lngLat);
          }

          var i = 0;
          var results;
          console.log("data.locations.length", data.locations.length);
          while (i < data.locations.length) {
            var j = i;
            // if (measure >= "0") {
            console.log("data has measure", data.locations[j].geometry.m);
            console.log(
              "headerContent",
              getHeaderContent(data.locations.length)
            );
            dojo.byId("resultsDivHeader").innerHTML = getHeaderContent(
              data.locations.length
            );
            // for (var j = 0; j < data.length; j++) {
            getCounty(data.locations[j].routeId.substring(0, 2));
            content += "<p>Route ID: " + data.locations[j].routeId;
            content +=
              "</p><p>Route: " +
              data.locations[j].routeId.substring(3, 7).replace(/^0+/, "");
            content +=
              "</p><p>Sub-Route: " +
              data.locations[j].routeId.substring(7, 9).replace(/^0+/, "");
            content += '</p><p>County: <span class="resultCounty"></span>';
            content +=
              '</p><p>Sign System: <span id="resultsSignSys">' +
              getSignSystem(data.locations[j].routeId.substring(2, 3)) +
              "</span>";
            content +=
              '</p><p>Supplemental Code: <span id="resultsSuppCode">' +
              getSupplementalCode(data.locations[j].routeId.substring(9, 11)) +
              "</span>";
            content +=
              '</p><p>Direction: <span id="resultsDirection">' +
              getDirection(data.locations[j].routeId.substring(11, 13));
            content += "</p><p>Measure: " + measure.toFixed(3);
            content += "</p><p>Long: " + lngLat[0].toFixed(6);
            content += "</p><p>Lat: " + lngLat[1].toFixed(6);
            if (data.locations.length > 1) {
              // if (j != data.length - 1) {
              content += "<hr />";
            }
            //  }
            //  }
            i++;
          }
        }
        console.log("content", content);
        break;
      case "esriGeometryPolyline":
        var polyData = {};
        polyData.X = data.locations[0].geometry.paths[0][0][0];
        polyData.Y = data.locations[0].geometry.paths[0][0][1];
        polyData.routeID = $("#routeID").val();
        polyData.LRSOp = "geometryToMeasure";
        polyData.Color = "green";
        polyData.eventType = "mapClick";
        console.log("polyData", polyData);
        getLRSData(polyData);
        break;
    }

    console.log("content", content);
    dojo.byId("resultsDivInner").innerHTML = content;
    dojo.byId("resultsDiv").style.display = "block";
  }

  function showG2MResults(data) {
    console.log("showG2MResults", data);
    console.log("data[0].measure", data[0].measure);
    var measure = data[0].measure;
    console.log("measure", measure);
    var headerContent = "",
      content = "",
      county = "";
    if (measure === undefined) {
      dojo.byId("resultsDivHeader").innerHTML = getHeaderContent(0);
      //   $("#resultsDivInner").hide();
    } else {
      $("#resultsDivInner").show();
      if (data.length >= 1) {
        lngLat = WebMercatorUtils.xyToLngLat(
          data[0].geometry.x,
          data[0].geometry.y
        );
        console.log("lngLat", lngLat);
      }

      var i = 0;
      var results;

      while (data[i]) {
        var j = i;
        // if (measure >= "0") {
        console.log("data has measure", data[i].measure);
        console.log("headerContent", getHeaderContent(data.length));
        dojo.byId("resultsDivHeader").innerHTML = getHeaderContent(data.length);
        // for (var j = 0; j < data.length; j++) {
        getCounty(data[j].routeId.substring(0, 2));
        content += "<p>Route ID: " + data[j].routeId;
        content +=
          "</p><p>Route: " + data[j].routeId.substring(3, 7).replace(/^0+/, "");
        content +=
          "</p><p>Sub-Route: " +
          data[j].routeId.substring(7, 9).replace(/^0+/, "");
        content += '</p><p>County: <span class="resultCounty"></span>';
        content +=
          '</p><p>Sign System: <span id="resultsSignSys">' +
          getSignSystem(data[j].routeId.substring(2, 3)) +
          "</span>";
        content +=
          '</p><p>Supplemental Code: <span id="resultsSuppCode">' +
          getSupplementalCode(data[j].routeId.substring(9, 11)) +
          "</span>";
        content +=
          '</p><p>Direction: <span id="resultsDirection">' +
          getDirection(data[j].routeId.substring(11, 13));
        content += "</p><p>Measure: " + data[j].measure.toFixed(3);
        content += "</p><p>Long: " + lngLat[0].toFixed(6);
        content += "</p><p>Lat: " + lngLat[1].toFixed(6);
        if (j != data.length - 1) {
          content += "<hr />";
        }
        //  }
        //  }
        i++;
      }
    }
    console.log("content", content);
    dojo.byId("resultsDivInner").innerHTML = content;
    dojo.byId("resultsDiv").style.display = "block";
  }
  // function showG2MResults(data, X, Y) {
  /* function showG2MResults(data) {
         console.log("showResults.data", data);
         var lngLat = WebMercatorUtils.xyToLngLat(data[0].geometry.x, data[0].geometry.y);
         console.log(lngLat);
         var locs = data;
         var headerContent = "";
         var content = "",
             county;
         var i = 0;
         var results;
         while (locs[i]) {
             if (locs[i]) {
                 console.log("headerContent", getHeaderContent(locs[i].length));
                 getCounty(locs[i].routeId.substring(0,2));
                 console.log("county", county);
                 content += "<p>Route ID: " + locs[i].routeId;
                 content += "</p><p>Route: " + locs[i].routeId.substring(3, 7).replace(/^0+/, '');
                 content += "</p><p>Sub-Route: " + locs[i].routeId.substring(7, 9).replace(/^0+/, '');
                 content += '</p><p>County: <span class="resultCounty"></span>';
                 content += '</p><p>Sign System: <span id="resultSignSys">' + getSignSystem(locs[i].routeId.substring(2, 3)) + '</span>';
                 content += '</p><p>Supplemental Code: <span id="resultsSuppCode">' + getSupplementalCode(locs[i].routeId.substring(9, 11)) + '</span>';
                 content += '</p><p>Direction: <span id="resultsDirection">' + locs[i].routeId.substring(11, 13);
                 content += "</p><p>Measure: " + locs[i].measure.toFixed(3) + '</span>';
                 content += "</p><p>Long: " + lngLat[0].toFixed(6);
                 content += "</p><p>Lat: " + " , " + lngLat[1].toFixed(6) + "</p>";
                 dojo.byId("resultsDivHeader").innerHTML = getHeaderContent(1);
                 dojo.byId("resultsDivInner").innerHTML = content;
                 dojo.byId("resultsDiv").style.display = "block";
     
                 console.log("content", content);
                 i++;
                // results = locs[i].results;

                 /*   if (results.length == 1) {
                        headerContent += '<div style="float: right"><button id="btnIconResults" class="btn btn-primary btn-xs" onclick=$("#resultsDivInner").toggle();$("#resultsTitle").toggle();><span id="iconResults" class="glyphicon glyphicon-resize-small" aria-hidden="true"></span></button></div><div id="resultsTitle" class="title">Location</div> &nbsp; ';
                    } else if (results.length > 1) {
                        headerContent += '<div style="float: right"><button id="btnIconResults" class="btn btn-primary btn-xs" onclick=$("#resultsDivInner").toggle();$("#resultsTitle").toggle();><span id="iconResults" class="glyphicon glyphicon-resize-small" aria-hidden="true"></span></button></div><div id="resultsTitle" class="title">Locations</div> &nbsp; ';
                    } else if (results.length == 0) {
                        headerContent = '<div style="float: right"><button id="btnIconResults" class="btn btn-primary btn-xs" onclick=$("#resultsDivInner").toggle();$("#resultsTitle").toggle();><span id="iconResults" class="glyphicon glyphicon-resize-small" aria-hidden="true"></span></button></div><div id="resultsTitle" class="title">No Routes Selected</div> &nbsp; ';
                    }
                    */

  /* for (var j = 0; j < results.length; j++) {
                       
                        getCounty(results[j].routeId.substring(0, 2));
                        //  getSignSystem(results[j].routeId.substring(2, 3));
                        console.log("county", county);
                        content += "<p>Route ID: " + results[j].routeId;
                        content += "</p><p>Route: " + results[j].routeId.substring(3, 7).replace(/^0+/, '');
                        content += "</p><p>Sub-Route: " + results[j].routeId.substring(7, 9).replace(/^0+/, '');
                        content += '</p><p>County: <span class="resultCounty"></span>';
                        content += '</p><p>Sign System: <span id="resultSignSys">' + getSignSystem(results[j].routeId.substring(2, 3)) + '</span>';
                        content += '</p><p>Supplemental Code: <span id="resultsSuppCode">' + getSupplementalCode(results[j].routeId.substring(9, 11)) + '</span>';
                        content += '</p><p>Direction: <span id="resultsDirection">' + results[j].routeId.substring(11, 13);
                        content += "</p><p>Measure: " + results[j].measure.toFixed(3) + '</span>';
                        content += "</p><p>Long / Lat: " + lngLat[0].toFixed(6) + " , " + lngLat[1].toFixed(6) + "</p>";
                        if (j != results.length - 1) {
                            content += "<hr />";
                        }

                    } 
                    
                    dojo.byId("resultsDivHeader").innerHTML = getHeaderContent(results.length);
                } else {
                    getCounty($("#routeID").val().substring(0, 2));
                    content += "<p>Route ID: " + $("#routeID").val();
                    content += "</p><p>Route: " + $("#routeID").val().substring(3, 7).replace(/^0+/, '');
                    content += "</p><p>Sub-Route: " + $("#routeID").val().substring(7, 9).replace(/^0+/, '');
                    content += '</p><p>County: <span class="resultCounty"></span>';
                    content += '</p><p>Sign System: <span id="resultSignSys">' + getSignSystem($("#routeID").val().substring(2, 3)) + '</span>';
                    content += '</p><p>Supplemental Code: <span id="resultsSuppCode">' + getSupplementalCode($("#routeID").val().substring(9, 11)) + '</span>';
                    content += '</p><p>Direction: <span id="resultsDirection">' + $("#routeID").val().substring(11, 13);
                    content += "</p><p>Measure: " + $("#routeID").val().toFixed(3) + '</span>';
                    content += "</p><p>Long / Lat: " + lngLat[0].toFixed(6) + " , " + lngLat[1].toFixed(6) + "</p>";

                }


                dojo.byId("resultsDivInner").innerHTML = content;
                dojo.byId("resultsDiv").style.display = "block";

                console.log("content", content);
                i++;

            }
            }
        }
    */
  function getHeaderContent(count) {
    console.log("getHeaderContent.count", count);
    var headerContent;
    switch (count) {
      case 0:
        headerContent =
          '<div style="float: right"><button id="btnIconResults" class="btn btn-primary btn-xs" onclick=$("#resultsDivInner").toggle();$("#resultsTitle").toggle();toggleIconResults();><span id="iconResults" class="glyphicon glyphicon-resize-small" aria-hidden="true"></span></button></div><div id="resultsTitle" class="title">No Route Selected</div> &nbsp; ';
        break;

      case 1:
        headerContent =
          '<div style="float: right"><button id="btnIconResults" class="btn btn-primary btn-xs" onclick=$("#resultsDivInner").toggle();$("#resultsTitle").toggle();toggleIconResults();><span id="iconResults" class="glyphicon glyphicon-resize-small" aria-hidden="true"></span></button></div><div id="resultsTitle" class="title">Location</div> &nbsp; ';
        break;

      default:
        headerContent =
          '<div style="float: right"><button id="btnIconResults" class="btn btn-primary btn-xs" onclick=$("#resultsDivInner").toggle();$("#resultsTitle").toggle();toggleIconResults();><span id="iconResults" class="glyphicon glyphicon-resize-small" aria-hidden="true"></span></button></div><div id="resultsTitle" class="title">Locations</div> &nbsp; ';
    }

    return headerContent;
  }

  function getCounty(countyCode) {
    var countyName, countyURL;
    countyURL = "https://gis.transportation.wv.gov/api/counties/" + countyCode;
    console.log(countyURL);
    $.getJSON(countyURL, null, function(data) {
      console.log("data", data);

      $.each(data, function(k, v) {
        //  console.log("countyName", v.CountyName);
        $(".resultCounty").html(v.CountyName);
      });
      //      console.log("result", data);
      //      console.log("countyName", data[0].CountyName);
      //      countyName = data[0].CountyName;
      //      return countyName;
    });
  }

  function getSignSystem(signSystemCode) {
    console.log("ssCode", signSystemCode);
    var signSys;
    switch (signSystemCode) {
      case "1":
        signSys = "Interstate";
        break;
      case "2":
        signSys = "US";
        break;
      case "3":
        signSys = "WV";
        break;
      case "4":
        signSys = "CO";
        break;
      case "5":
        signSys = "N/A";
        break;
      case "6":
        signSys = "State Park / Forest Rd";
        break;
      case "7":
        signSys = "FANS";
        break;
      case "8":
        signSys = "HARP";
        break;
      case "9":
        signSys = "OTHER";
        break;
      case "0":
        signSys = "MNS";
        break;
    }
    console.log("signSys", signSys);
    //  $("#resultSignSys").html(signSys);
    return signSys;
  }

  function getSupplementalCode(supplementalCode) {
    console.log("supplementalCode", supplementalCode);
    var suppCode;
    switch (supplementalCode) {
      case "00":
        suppCode = "N/A";
        break;
      case "01":
        suppCode = "Alternate";
        break;
      case "02":
        suppCode = "Wye";
        break;
      case "03":
        suppCode = "Spur";
        break;
      case "04":
        suppCode = "North";
        break;
      case "05":
        suppCode = "South";
        break;
      case "06":
        suppCode = "East";
        break;
      case "07":
        suppCode = "West";
        break;
      case "08":
        suppCode = "Business";
        break;
      case "09":
        suppCode = "North Bound (Business)";
        break;
      case "10":
        suppCode = "South Bound (Business)";
        break;
      case "11":
        suppCode = "East Bound (Business)";
        break;
      case "12":
        suppCode = "West Bound (Business)";
        break;
      case "13":
        suppCode = "Truck Route";
        break;
      case "14":
        suppCode = "Bypass";
        break;
      case "15":
        suppCode = "Loop";
        break;
      case "16":
        suppCode = "Toll";
        break;
      case "17":
        suppCode = "Ramp";
        break;
      case "18":
        suppCode = "Other";
        break;
      case "19":
        suppCode = "City Streets Non-State";
        break;
      case "20":
        suppCode = "Roads Under construction";
        break;
      case "21":
        suppCode = "Footbridges";
        break;
      case "22":
        suppCode = "Historical Bridges";
        break;
      case "23":
        suppCode = "Connector";
        break;
      case "24":
        suppCode = "New/Proposed";
        break;
      case "25":
        suppCode = "Crossover (btwn dual geometry)";
        break;
      case "26":
        suppCode = "Emergency Crossover";
        break;
      case "27":
        suppCode = "Left Turn Lane";
        break;
      case "28":
        suppCode = "Right Turn Lane";
        break;
      case "99":
        suppCode = "Abandoned";
        break;
    }
    console.log("suppCode", suppCode);
    return suppCode;
  }

  function getDirection(direction) {
    switch (direction) {
      case "00":
        direction = "Undivided";
        break;
      default:
        direction = direction;
        break;
    }
    return direction;
  }

  function padRoute(size, value, textbox) {
    console.log("size", size);
    console.log("value", value);
    console.log("textbox", textbox);
    var i = 0;
    var pad = "";
    while (i < size - value.length) {
      pad += "0";
      i++;
    }
    console.log("PadValue", pad + value);
    //document.getElementById(textbox).value = pad + value;
    $(textbox).val(pad + value);
  }

  // ---------------------------------------------------------------
  $("#legendButton").html(
    '<div class="esri-component esri-widget-button esri-widget" role="button" tabindex="0" aria-label="Toggle Legend" title="Toggle Legend"><span class="esri-icon-layer-list esri-icon" aria-hidden="true" onclick=$("#legendDiv").toggle();></span></div>'
  );
  var legendButton = dom.byId("legendButton");
  view.ui.add(legendButton, "top-left");
  // createLegend();
}); // END of require
